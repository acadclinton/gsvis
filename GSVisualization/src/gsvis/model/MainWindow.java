package gsvis.model;

import gsvis.logic.Problem;
import gsvis.logic.SchemaNode;


import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JInternalFrame;

public class MainWindow implements ActionListener {

	private JFrame frmMaxcutVisualization;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmMaxcutVisualization.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	
	private int width=1366;
	private int height=768;
	private String inputpath="input.txt";
	private void initialize() {
		frmMaxcutVisualization = new JFrame();
		frmMaxcutVisualization.setTitle("Maxcut Visualization");
		//frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		//frame.setSize(500, 300);
		frmMaxcutVisualization.setBounds(50, 50, 100+width, 100+height);
		frmMaxcutVisualization.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMaxcutVisualization.getContentPane().setLayout(new BorderLayout(0, 0));
		
		
		
		
		
		JMenuBar menuBar = new JMenuBar();
		frmMaxcutVisualization.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		mnFile.setMnemonic(KeyEvent.VK_F);
		JMenuItem mntmLoadInput = new JMenuItem("Load Input");
		mntmLoadInput.setMnemonic(KeyEvent.VK_L);
		mntmLoadInput.setActionCommand("load");
		mntmLoadInput.addActionListener(this);
		
		mnFile.add(mntmLoadInput);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		mntmExit.setMnemonic(KeyEvent.VK_X);
		
		
		
		
		
	}
	
	
	final JFileChooser fc = new JFileChooser();
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("load"))
		{
			String inputpath="input.txt";
			 int returnVal = fc.showOpenDialog(null);
			 if (returnVal == JFileChooser.APPROVE_OPTION) {
				 File selectedFile =	 fc.getSelectedFile();
				 System.out.println("Selected: " + selectedFile.getParent()	 + " --- "	 + selectedFile.getName());
				 inputpath=selectedFile.getParent()+"/"+selectedFile.getName();
			} 
			 else return;
			VisualizeFrame loadedFrame;
			try {
				loadedFrame = new VisualizeFrame(inputpath,inputpath);
				frmMaxcutVisualization.getContentPane().add(loadedFrame, BorderLayout.CENTER);
				loadedFrame.setVisible(true);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			frmMaxcutVisualization.revalidate();
			frmMaxcutVisualization.repaint();
		}
	}
}
