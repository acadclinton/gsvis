package gsvis.model;

import gsvis.logic.Problem;
import gsvis.logic.Schema;
import gsvis.logic.SchemaNode;
import gsvis.logic.SearchAlg;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.List;
import java.awt.Toolkit;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JToolBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

public class VisualizeFrame extends JInternalFrame implements MouseListener{
	private JTextField textField;


	Surface schemaMap;
	private int width=1024;
	private int height=768;
	private String inputpath;
	/**
	 * Create the this.
	 */
	private void initProblem()
	{
	}
	Problem p;
	
	SchemaNode root;
	Vector<Schema> tabooList;
	public VisualizeFrame(String title,String inputpath) throws Exception {
		
		super(title);
		this.addMouseListener(this);
		setResizable(true);
		setMaximizable(true);
		setClosable(true);
		
		this.inputpath=inputpath;
		setBounds(100, 100, 450, 300);
		
		
		this.setBounds(50, 50, 100+width, 100+height);
		this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout(0, 0));
		
		try {
			p=new Problem(inputpath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(this,"Wrong input data.","Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			this.dispose();
			
		}
		SearchAlg s = new SearchAlg(p);
		
		String generalSchema="";
		for(int i=0;i<p.vnum;i++)
		{
			generalSchema+="*";
		}
		root=new SchemaNode(generalSchema,s);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(0, 0));
		this.setContentPane(panel);
		

	    //first button
	    
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		
		
		
		panel.add(toolBar, BorderLayout.NORTH);
		
		JButton btnTaboo = new JButton("Taboo");
		btnTaboo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(tb==null)
				{
					tb=new TabooDialog(tabooList,treeview.root_node_model);
					tb.pack();
					tb.setVisible(true);
				}
				else if(!tb.isDisplayable())
				{					
					tb=null;
					System.gc();
					tb=new TabooDialog(tabooList,treeview.root_node_model);
					tb.pack();
					tb.setVisible(true);

				}
				else 
				{
					tb.setVisible(false);
					tb.dispose();
					
				}
				
			}
		});
		toolBar.add(btnTaboo);
		toolBar.addSeparator();
		JButton btnRun = new JButton("Run for generations");
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(rd==null)
				{
					rd=new RunDialog(root,tabooList,treeview);
					rd.pack();
					rd.setVisible(true);
				}
				else if(!rd.isDisplayable())
				{
					rd=null;
					System.gc();
					rd=new RunDialog(root,tabooList,treeview);
					rd.pack();
					rd.setVisible(true);
				}
				else 
				{
					rd.setVisible(false);
					rd.dispose();
					
				}
			}
		});
		toolBar.add(btnRun);
		toolBar.addSeparator();
		
		final JButton btnRunUntilNew = new JButton("Run until new best result");
		btnRunUntilNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final SearchAlg s=root.getSearchInstance();
				Double rootbestfit=root.getModel().getBest();
				long start=System.currentTimeMillis();
				Integer cnt=0;
				while(true)
				{
					s.onestep();
					cnt++;
			  		int t=s.get_best_fval();
			  		if(rootbestfit<(double)t)
			  		{
			  			rootbestfit=new Double(t);
			  			JOptionPane.showMessageDialog(btnRunUntilNew,"Found "+String.format("%.2f", rootbestfit)+"\n"+cnt.toString()+" generations","No better value", JOptionPane.OK_OPTION);
			  			
			  			break;
			  		}
			  		long current=System.currentTimeMillis();
			  		long elapsed=current-start;
			  		if(elapsed>1000*60)
			  		{
			  			JOptionPane.showMessageDialog(btnRunUntilNew,"There is no better solution found in one-minute-search.\n"+cnt.toString()+" generations","No better value", JOptionPane.ERROR_MESSAGE);
			  			break;
			  		}
			  	}
				treeview.reloadTreeView(root, treeview.ml);
			}
		});
		
		toolBar.add(btnRunUntilNew);
		toolBar.addSeparator();
		
		lblBest = new JLabel("Best:");
		toolBar.add(lblBest);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));
		JPanel panel_1 = new JPanel();
		panel_2.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		schemaMap = new Surface(root.getSchema());
		schemaMap.mapsize=p.vnum;
		panel_1.add(schemaMap, BorderLayout.CENTER);
		panel_1.setPreferredSize(new Dimension(width,50));
		JLabel lblSchemaMap = new JLabel("Schema Map:");
		lblSchemaMap.setPreferredSize(new Dimension(100,50));
		//schemaMap.setBounds(100, 0, width, 50);
		panel_1.add(lblSchemaMap,BorderLayout.WEST);
		treeview = new TreeView(root,this,lblBest);
		treeview.addMouseListener(this);
		treeview.setProblem(p);
		treeview.panelwidth=width-300;
		treeview.panelheight=height-100;
		
		JScrollPane scrollPane = new JScrollPane(treeview);
		panel_2.add(scrollPane, BorderLayout.CENTER);
		treeview.setLayout(null);
		treeview.setBounds(0, 0, treeview.panelwidth,treeview.panelheight);
		treeview.setPreferredSize(new Dimension(treeview.panelwidth*5,treeview.panelheight*3));
		tabooList=new Vector<Schema>();
		treeview.setTabooList(tabooList);
		this.pack();
		
	}
	JLabel lblBest;
	Integer rootbestfit=0;
	TreeView treeview;
	TabooDialog tb=null;
	RunDialog rd=null;
	NodeModel selected=null;
	@Override
	public void mouseClicked(MouseEvent m) {
		// TODO Auto-generated method stub
		if(m.getSource().getClass().equals(NodeModel.class))
		{
			NodeModel node=(NodeModel)m.getSource();
			Schema schema=node.getNode().getSchema();
			System.out.print(schemaMap.isVisible());
			schemaMap.setSchema(schema);
			schemaMap.setVisible(true);
			if(selected!=null)selected.unselect();
			selected=node;
			selected.select();
			schemaMap.invalidate();
			schemaMap.repaint();
		}
		else
		{
			if(selected!=null)selected.unselect();
			schemaMap.setSchema(null);
			schemaMap.invalidate();
			schemaMap.repaint();
		}
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	 @Override
	 public void paintComponent(Graphics g) {

	        super.paintComponent(g);
	        System.out.print("dd");
	        //doDrawing(g);
	    }
}
