package gsvis.model;

import gsvis.logic.Schema;
import gsvis.logic.SchemaNode;
import gsvis.logic.SearchAlg;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.JProgressBar;

public class RunDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField timeField;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		try {
			RunDialog dialog = new RunDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	SchemaNode root;
	TreeView v;
	public RunDialog(SchemaNode rt, Vector<Schema>taboo,TreeView view) {
		this.v=view;

		this.root=rt;
		setModal(true);
		setResizable(false);
		setTitle("Run");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.NORTH);
			panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			{
				JLabel lblRunUnderTaboo = new JLabel("Run under taboo constraints");
				panel.add(lblRunUnderTaboo);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			{
				timeField = new JTextField(5);
				timeField.setText("10000");
				panel.add(timeField);
			}
			
			{
				JLabel lblSecond = new JLabel("generation");
				panel.add(lblSecond);
			}
			panel.setMaximumSize(new Dimension(200,50));
			panel.setMinimumSize(new Dimension(200,50));
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.SOUTH);
			panel.setMaximumSize(new Dimension(200,50));
			panel.setMinimumSize(new Dimension(200,50));
			panel.setLayout(new BorderLayout(0, 0));
			{
				JPanel panel_1 = new JPanel();
				panel.add(panel_1, BorderLayout.SOUTH);
				panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			}
			{
				JPanel panel_1 = new JPanel();
				panel.add(panel_1, BorderLayout.NORTH);
				{
					progressBar = new JProgressBar();
					progressBar.setMinimum(0);
					progressBar.setMaximum(100);
					progressBar.setValue(0);
					panel_1.add(progressBar);
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			JButton closeButton = new JButton("Close");
			closeButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			JButton btnRun = new JButton("Run");
			buttonPane.add(btnRun);
			btnRun.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					final int rungen=Integer.parseInt(timeField.getText());
					
					progressBar.setMaximum(rungen);
					progressBar.setValue(0);
					v.closeNode(root.getModel());
					final SearchAlg s=root.getSearchInstance();
					gen=0;
					Thread th=new Thread(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							for(int i=0;i<rungen;i++)
							{
								gen++;
								s.onestep();
						  		progressBar.setValue(gen);
						  		repaint();
						  	}
							
							v.reloadTreeView(root, v.ml);

						}
						
					});
					th.start();
				}});
				
				closeButton.setActionCommand("Cancel");
				buttonPane.add(closeButton);
			}
		
		this.pack();
	}
	int gen=0;
	JProgressBar progressBar;
}
