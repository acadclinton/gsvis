package gsvis.model;

import gsvis.logic.Schema;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

public class TabooDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		try {
			TabooDialog dialog = new TabooDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	public Vector<Schema> tabooList;
	public Vector<String> listtext;
	public TabooDialog(Vector taboo,final NodeModel root) {
		tabooList=taboo;
		final TabooDialog th=this;
		listtext=new Vector<String>();
		for(Schema i : tabooList)
		{
			String text=i.toString();
			Double avg=i.get_avg_fitness();
			Double max=i.get_best_fitness();
			text="AVG:"+String.format("%.2f",avg)+" BEST:"+max.toString()+"   "+text.substring(0,20);
			 
			listtext.add(text);
		}
		setModal(true);
		setResizable(false);
		setTitle("Taboo Management");
		setBounds(100, 100, 450, 300);
		setMinimumSize(new Dimension(200,200));
		setMaximumSize(new Dimension(200,500));
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		final JList list = new JList();
		list.setListData(listtext);
		list.setVisibleRowCount(5);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		contentPanel.add(list, BorderLayout.CENTER);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton removeButton = new JButton("Remove");
		removeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			int del=list.getSelectedIndex();
			Schema delSchema=tabooList.get(del);
			NodeModel apnode=root.findSchema(delSchema);
			if(apnode!=null)apnode.unban();
			tabooList.remove(del);
			listtext.remove(del);
			th.revalidate();
			th.repaint();
			}
		});
		buttonPane.add(removeButton);
		JButton closeButton = new JButton("Close");
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		closeButton.setActionCommand("OK");
		buttonPane.add(closeButton);
		getRootPane().setDefaultButton(closeButton);
	}

}
