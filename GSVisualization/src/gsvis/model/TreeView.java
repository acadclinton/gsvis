package gsvis.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Vector;

import gsvis.logic.Problem;
import gsvis.logic.Schema;
import gsvis.logic.SchemaNode;
import gsvis.logic.SearchAlg;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class TreeView extends JPanel implements ActionListener{
	
	private int level=0;
	private int spacex=250;
	private int spacey=150;
	private int nodew=205;
	private int nodeh=130;
	private int height=600;
	public int panelwidth;
	public int panelheight;
	
	public SchemaNode root;
	ArrayList<NodeModel> nodelist;
	Vector<Schema> tabooList;
	NodeModel root_node_model;
	public MouseListener ml;
	
	public void setTabooList(Vector<Schema> a)
	{
		tabooList=a;
	}
	JLabel bestLabel;
	public TreeView(SchemaNode rt, MouseListener ml,JLabel bestlabel)
	{
		bestLabel=bestlabel;
		this.ml=ml;
		this.setOpaque(false);
		this.setBackground(Color.WHITE);
		root=rt;
		nodelist=new ArrayList<NodeModel>();
		
		root_node_model=new NodeModel(root,50,height/2,level,bestLabel);
		
		
		root_node_model.setBounds(root_node_model.x, root_node_model.y,nodew, nodeh);
		nodelist.add(root_node_model);
		root_node_model.setId(nodelist.indexOf(root_node_model));
		root_node_model.setListener(this);
		root_node_model.addMouseListener(ml);
		//addTree(root_node_model);
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		        addComponent();
		    }
		});
	}
	public void reloadTreeView(SchemaNode rt, MouseListener ml)
	{
		for(NodeModel i : nodelist)
		{
			this.remove(i);
		}
		nodelist.clear();
		nodelist=null;
		root=null;
		System.gc();
		this.ml=ml;
		this.setOpaque(false);
		this.setBackground(Color.WHITE);
		root=rt;
		root.clearChild();
		nodelist=new ArrayList<NodeModel>();
		root_node_model=new NodeModel(root,50,height/2,level,bestLabel);
		root_node_model.setBounds(root_node_model.x, root_node_model.y,nodew, nodeh);
		nodelist.add(root_node_model);
		root_node_model.setId(nodelist.indexOf(root_node_model));
		root_node_model.setListener(this);
		root_node_model.addMouseListener(ml);
		
		//addTree(root_node_model);
		NodeModel node=(NodeModel)root_node_model;
		SearchAlg s=node.getNode().getSearchInstance();
		s.set_capacity(1000);
		long l = System.currentTimeMillis();
		Schema[] schema = s.get_schema_cluster();
		for(int i=0;i<schema.length-1;i++){
			for(int j=i;j<schema.length;j++)
			{
				if(schema[i].get_avg_fitness()<schema[j].get_avg_fitness())
				{
					Schema tt=schema[i];
					schema[i]=schema[j];
					schema[j]=tt;
				}
			}
		}
		try {
			addTree(node,schema);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int t=root.getSearchInstance().get_best_fval();
		root.getModel().setBest((double)t);
		root_node_model.btnSearch.setText("Close");
		root_node_model.btnSearch.setActionCommand("close");
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		        addComponent();
		    }
		});
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		        addComponent();
		    }
		});
	}
	public void paintComponent(Graphics g) {
		//super.paintComponents(g);
		for(final NodeModel r : nodelist) {
			
			if(r.isVisable())
				if(r.getParentalLine()!=null)
					r.getParentalLine().paint(g);
		}
	}
	Problem p;
	public void setProblem(Problem p)
	{
		this.p=p;
	}
	private void addTree(NodeModel node,Schema[] schemacluster) throws Exception
	{
		level++;
		SchemaNode[] childs=new SchemaNode[schemacluster.length];
		for(int i=0;i<schemacluster.length;i++)
		{
			SearchAlg s=node.getNode().getSearchInstance().subproblem(schemacluster[i]);
			childs[i]=new SchemaNode(schemacluster[i],s);
			node.getNode().addChild(childs[i]);
		}
		int cn=node.getNode().getChildNum();
		if(cn==0)
		{
			return;
		}
		for(int i=0;i<cn;i++)
		{
			int parenty=node.y;
			
			int py=(parenty-(int) (((double)(cn-1)/2.0)*spacey));
			if(py<=10)py=10;
			if(cn%2==0)
			{
				py+=(int)(i*spacey);
			}
			else
			{
				py+=(int)(i*spacey);
			}
			Line line;

			NodeModel node_model=new NodeModel(node.getNode().getChild(i),node.x+spacex,py,level);
			line = new Line(node.x+nodew,node.y+nodeh/2,node_model.x,node_model.y+nodeh/2);
			node_model.setParentalLine(line);
			node_model.setBounds(node_model.x, node_model.y,nodew, nodeh);
			nodelist.add(node_model);
			node_model.setId(nodelist.indexOf(node_model));
			node_model.setListener(this);
			node_model.addMouseListener(ml);
			
		}
		SchemaNode grandparent=node.getNode().getParent();
		if(grandparent!=null)
		{
			for(NodeModel i : nodelist)
			{
				SchemaNode i_parent=i.getNode().getParent();
				if(i_parent!=null)
				{
					if(grandparent.equals(i_parent) && !i.equals(node))
					{
						
						i.disappear();
					}
				}
			}
		}
		
		
	}

	
	private static final long serialVersionUID = 1L;
	private void closeSubTree(NodeModel node)
	{
		
		SchemaNode scnode=node.getNode();
		for(int i=0;i<scnode.getChildNum();i++)
		{
			SchemaNode child=scnode.getChild(i);
			if(child.getModel()!=null)
			{
				NodeModel childModel=child.getModel();
				closeSubTree(childModel);
				childModel.clear();
				nodelist.remove(childModel);
			}
		}
		scnode.resetChild();
		
	}
	public void closeNode(NodeModel node)
	{
		SchemaNode scnode=node.getNode();
		closeSubTree(node);
		if(scnode.getParent()!=null)
		{
			for(NodeModel i:nodelist)
			{
				SchemaNode sc_i=i.getNode();
				if(sc_i.getParent()!=null)
				{
					if(sc_i.getParent().equals(scnode.getParent()))
					{
						i.appear();
					}
				}
			}
		}
		JButton t=node.btnSearch;
		t.setText("Search");
		t.setActionCommand(new Integer(nodelist.indexOf(node)).toString()+" search");
		System.gc();
		this.revalidate();
		this.repaint();
	}
	private int searchgeneration=10000;
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton t=(JButton)e.getSource();
		if(e.getActionCommand().equals("close"))
		{
			NodeModel node=(NodeModel)t.getParent();
			closeNode(node);
			return;
		}
		else if(e.getActionCommand().equals("ban"))
		{
			NodeModel node=(NodeModel)t.getParent();
			if(!node.isBanned())
			{
				closeNode(node);
				tabooList.add(node.getNode().getSchema());
				node.ban();
			}
			else
			{
				
				node.unban();
				tabooList.remove(node.getNode().getSchema());
				this.revalidate();
				this.repaint();
			}
			
			return;
		}
		else
		{
			NodeModel node=(NodeModel)t.getParent();
			
			SearchAlg s=node.getNode().getSearchInstance();
			s.set_capacity(1000);
			for (int i = 0; i < searchgeneration; i++) {
				s.onestep();
			}
			
			long l = System.currentTimeMillis();
			Schema[] schema = s.get_schema_cluster();
			for(int i=0;i<schema.length-1;i++){
				for(int j=i;j<schema.length;j++)
				{
					if(schema[i].get_avg_fitness()<schema[j].get_avg_fitness())
					{
						Schema tt=schema[i];
						schema[i]=schema[j];
						schema[j]=tt;
					}
				}
			}
			System.out.println(System.currentTimeMillis() - l + "ms");
			try {
				addTree(node,schema);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			int tt=root.getSearchInstance().get_best_fval();
			root.getModel().setBest((double)tt);
			t.setText("Close");
			t.setActionCommand("close");
			SwingUtilities.invokeLater(new Runnable() {
			    public void run() {
			        addComponent();
			    }
			});
		}
		
	}
	public void addComponent()
	{
		for(NodeModel i:nodelist)
		{
			if(!this.isAncestorOf(i))
			{
				this.add(i);
				
			}
		}
		this.validate();
		this.repaint();
		
	}
	

	
}
