package gsvis.model;

import gsvis.logic.Schema;
import gsvis.logic.SchemaNode;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.util.Random;

import javax.swing.JPanel;

public class Surface extends JPanel{
	
	
	private byte[] schema;
	private int[] colormap;
	private int n;
	public int mapsize;
	private int pixel;
	private int barheight=30;
	
	public Surface(Schema schema)
	{
		if(schema==null){
			this.schema=null;
			return;
		}
			
		this.schema=schema.get_schema();
		n=this.schema.length;
		if(n>800)pixel=1;
		else if(n>400)pixel=2;
		else if(n>200)pixel=4;
		else if(n>100)pixel=8;
		else if(n>50)pixel=16;
		else pixel=32;
		colormap=new int[mapsize];
		for(int i=0;i<n;i+=1)
		{
			if(this.schema[i]==2)
			{
				colormap[i]=3;
			}
			else if(this.schema[i]==1)
			{
				colormap[i]=0;
			}
			else
				colormap[i]=2;
		}
		
	}
	public void setSchema(Schema schema)
	{
		if(schema==null)
		{
			this.schema=null;
			return;
			
		}
		this.schema=schema.get_schema();
		n=this.schema.length;
		if(n>800)pixel=1;
		else if(n>400)pixel=2;
		else if(n>200)pixel=4;
		else if(n>100)pixel=8;
		else if(n>50)pixel=16;
		else pixel=32;
		colormap=new int[mapsize];
		for(int i=0;i<n;i+=1)
		{
			if(this.schema[i]==2)
			{
				colormap[i]=3;
			}
			else if(this.schema[i]==1)
			{
				colormap[i]=0;
			}
			else
				colormap[i]=2;
		}
		
	}
	private void doDrawing(Graphics g) {
		 Graphics2D g2d = (Graphics2D) g;
	        g2d.setColor(Color.BLUE);
	        
	        for (int i = 0; i <mapsize; i++) {
	        	if(schema==null)
	        	{
	        		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1.0f-(3 * 0.33f)));
	        		g2d.fillRect(i*pixel, 10, pixel, barheight);
	        	}
	        	else
	        	{
	        		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1.0f-(colormap[i] * 0.33f)));
	        		g2d.fillRect(i*pixel, 10, pixel, barheight);
	        	}
	        }        
		
	}

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        doDrawing(g);
    }

}
