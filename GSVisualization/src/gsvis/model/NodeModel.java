package gsvis.model;

import gsvis.logic.Schema;
import gsvis.logic.SchemaNode;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class NodeModel extends JPanel implements ActionListener{

	private SchemaNode node;
	private boolean banned=false;
	private Line parental_line;
	public Integer x;
	public Integer y;
	private Integer id;
	public int level;
	private boolean appearance;
	public NodeModel findSchema(Schema ss)
	{
		SchemaNode sc=this.getNode();
		if(ss.equals(sc.getSchema()))return this;
		for(int i=0;i<sc.getChildNum();i++)
		{
			NodeModel tt=sc.getChild(i).getModel().findSchema(ss);
			if(tt!=null)return tt;
		}
		return null;
	}
	public void setId(int i){
		id=i;
	}
	public Integer getId()
	{
		return id;
	}
	public SchemaNode getNode()
	{
		return node;
	}
	public int getLevel()
	{
		return level;
	}
	public void setParentalLine(Line l)
	{
		parental_line=l;
	}
	public Line getParentalLine()
	{
		return parental_line;
	}
	public boolean isVisable()
	{
		return appearance;
	}
	public void disappear()
	{
		if(appearance)
		{
			this.setVisible(false);
			this.parental_line.setVisible(false);
			appearance=false;
		}
	}
	
	public void appear()
	{
		if(!appearance)
		{
			this.setVisible(true);
			this.parental_line.setVisible(true);
			appearance=true;
		}
	}
	
	public void clear()
	{
		this.getParent().remove(this);
		node.setModel(null);
		node=null;
		parental_line=null;
		x=null;y=null;id=null;
		
		System.gc();
	}
	private Double best;
	JLabel bestlabel;
	public void setBest(Double t)
	{
		
		best=t;
		bestlabel.setText("Best:"+best.toString());
		lblbestvalue.setText(best.toString());
	}
	public Double getBest()
	{
		return best;
	}
	JLabel lblbestvalue;
	/**
	 * @wbp.parser.constructor
	 */
	public void select()
	{
		setBackground(Color.YELLOW);
	}
	public void unselect()
	{
		if(this.isBanned())
		{
			setBackground(new Color(255, 160, 122));
		}
		else setBackground(new Color(135, 206, 250));
		
	}
	/**
	 * @wbp.parser.constructor
	 */
	public NodeModel(SchemaNode n,int x,int y,int level) {
		appearance=true;
		setBackground(new Color(135, 206, 250));
		setBorder(UIManager.getBorder("CheckBox.border"));
		this.x=x;
		this.y=y;
		node=n;
		n.setModel(this);
		setLayout(null);
		
		JLabel lblSchema = new JLabel("Schema");
		lblSchema.setBounds(10, 9, 73, 15);
		add(lblSchema);
		
		JLabel label_schema_value = new JLabel("010");
		label_schema_value.setBounds(91, 9, 65, 15);
		
		label_schema_value.setText(node.getSchemaString());
		add(label_schema_value);
		
		JLabel lblFitness = new JLabel("AVG");
		lblFitness.setBounds(10, 31, 73, 15);
		add(lblFitness);
		
		
		JLabel label_fitness_value = new JLabel("35");
		label_fitness_value.setBounds(91, 31, 65, 15);
		label_fitness_value.setText(node.getAvg().toString());
		
		add(label_fitness_value);
		
		btnSearch = new JButton("Search");
		btnSearch.setLocation(10, 95);
		btnSearch.setSize(80, 23);
		
		
		
		
		
		
		add(btnSearch);
		
		btnBan = new JButton("Ban");
		btnBan.setLocation(116, 95);
		btnBan.setSize(80, 23);
		
		
		
		add(btnBan);
		
		JLabel lblBest = new JLabel("Best");
		lblBest.setBounds(10, 50, 57, 15);
		add(lblBest);
		
		lblbestvalue = new JLabel("best");
		lblbestvalue.setBounds(91, 50, 57, 15);
		add(lblbestvalue);
		lblbestvalue.setText(node.getBest().toString());
		best=0.0;
		if(node.getSchema()==null)
		{
			//lblBest.setVisible(false);
			lblbestvalue.setText(best.toString());
			
			lblFitness.setVisible(false);
			label_fitness_value.setVisible(false);
			btnBan.setVisible(false);
		}
		JLabel lblDontCare = new JLabel("Don't Care");
		lblDontCare.setBounds(10, 70, 73, 15);
		add(lblDontCare);
		
		JLabel lblDontcarevalue = new JLabel("New label");
		lblDontcarevalue.setText(node.dontcare().toString());
		lblDontcarevalue.setBounds(91, 75, 57, 15);
		add(lblDontcarevalue);
		

	}
	public NodeModel(SchemaNode n,int x,int y,int level,JLabel best) {
		bestlabel=best;
		appearance=true;
		setBackground(new Color(135, 206, 250));
		setBorder(UIManager.getBorder("CheckBox.border"));
		this.x=x;
		this.y=y;
		node=n;
		n.setModel(this);
		setLayout(null);
		
		JLabel lblSchema = new JLabel("Schema");
		lblSchema.setBounds(10, 9, 73, 15);
		add(lblSchema);
		
		JLabel label_schema_value = new JLabel("010");
		label_schema_value.setBounds(91, 9, 65, 15);
		
		label_schema_value.setText(node.getSchemaString());
		add(label_schema_value);
		
		JLabel lblFitness = new JLabel("AVG");
		lblFitness.setBounds(10, 31, 73, 15);
		add(lblFitness);
		
		
		JLabel label_fitness_value = new JLabel("35");
		label_fitness_value.setBounds(91, 31, 65, 15);
		label_fitness_value.setText(node.getAvg().toString());
		
		add(label_fitness_value);
		
		btnSearch = new JButton("Search");
		btnSearch.setLocation(10, 95);
		btnSearch.setSize(80, 23);
		
		
		
		
		
		
		add(btnSearch);
		
		btnBan = new JButton("Ban");
		btnBan.setLocation(116, 95);
		btnBan.setSize(80, 23);
		
		
		
		add(btnBan);
		
		JLabel lblBest = new JLabel("Best");
		lblBest.setBounds(10, 50, 57, 15);
		add(lblBest);
		
		lblbestvalue = new JLabel("best");
		lblbestvalue.setBounds(91, 50, 57, 15);
		add(lblbestvalue);
		lblbestvalue.setText(node.getBest().toString());
		this.best=0.0;
		if(node.getSchema()==null)
		{
			//lblBest.setVisible(false);
			lblbestvalue.setText(this.best.toString());
			
			lblFitness.setVisible(false);
			label_fitness_value.setVisible(false);
			btnBan.setVisible(false);
		}
		JLabel lblDontCare = new JLabel("Don't Care");
		lblDontCare.setBounds(10, 70, 73, 15);
		add(lblDontCare);
		
		JLabel lblDontcarevalue = new JLabel("New label");
		lblDontcarevalue.setText(node.dontcare().toString());
		lblDontcarevalue.setBounds(91, 75, 57, 15);
		add(lblDontcarevalue);
		

	}
	JButton btnSearch;
	JButton btnBan;
	public ActionListener clickListener;
	public void setListener(ActionListener l)
	{
		clickListener=l;
		btnSearch.setActionCommand("search");
		btnSearch.addActionListener(clickListener);
		btnBan.setActionCommand("ban");
		btnBan.addActionListener(clickListener);
	}
	public boolean isBanned()
	{
		return banned;
	}
	public void ban()
	{
		if(banned==false)
		{
			setBackground(new Color(255, 160, 122));
			btnSearch.setText("Banned");
			btnSearch.setVisible(false);
			banned=true;
		}
	}
	public void unban()
	{
		if(banned==true)
		{
			setBackground(new Color(135, 206, 250));
			btnSearch.setText("Search");
			btnSearch.setVisible(true);
			banned=false;
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand()=="ban")
		{
			
		}
	}
}
