package gsvis.logic;

import java.io.*;
import java.util.*;

public class Problem {
	public final Random R;
	public final int vnum;
	public final int[] deg;
	public final int[][] adj, weight;
	
	public Problem(String inputfile, long seed) throws Exception {
		// SET RANDOM SEED
		R = new Random(seed);
		
		// LOAD INPUT
		Scanner sc = new Scanner(new File(inputfile));
		vnum = sc.nextInt();
		int e = sc.nextInt();
		deg = new int[vnum];
		adj = new int[vnum][];
		weight = new int[vnum][];
		for (int i = 0; i < vnum; i++) {
			adj[i] = new int[vnum];
			weight[i] = new int[vnum];
		}
		for (int i = 0; i < e; i++) {
			int a = sc.nextInt(), b = sc.nextInt(), c = sc.nextInt();
			a--; b--;
			adj[a][deg[a]] = b; adj[b][deg[b]] = a;
			weight[a][b] = c;
			weight[b][a] = c;
			deg[a]++; deg[b]++;
		}
		sc.close();
	}
	
	public Problem(String inputfile) throws Exception {
		this(inputfile, System.currentTimeMillis());
	}
}
