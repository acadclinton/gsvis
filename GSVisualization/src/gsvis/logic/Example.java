package gsvis.logic;
public class Example {
	
	public static void run(SearchAlg s) {
		int best = s.get_best_fval();
		System.out.println(s.get_generation_num()  + " " + best);
		while (true) {
			s.onestep();
			if (best < s.get_best_fval()) {
				best = s.get_best_fval();
				System.out.println(s.get_generation_num() + " " + best);
				if (best == 3314) break;
			}
		}
		print_sol(s.get_best_sol());
	}
	
	public static void run(SearchAlg s, int gen) {
		int best = s.get_best_fval();
		System.out.println(s.get_generation_num()  + " " + best);
		while (gen > 0) {
			s.onestep();
			if (best < s.get_best_fval()) {
				best = s.get_best_fval();
				System.out.println(s.get_generation_num() + " " + best);
			}
			gen--;
		}
	}
	
	public static void routine(Problem p) throws Exception {
		SearchAlg s = new SearchAlg(p);
		run(s, 500000);
		Thread.sleep(500);
		Schema[] cluster = s.get_schema_cluster();
		print_cluster(cluster);
		Thread.sleep(500);
		SearchAlg s2 = s.subproblem(cluster[0]);
		run(s2, 900000);
		s.add_tabu(cluster[0]);
	}
	
	public static void print_sol(byte[] sol) {
		for (int i = 0; i < sol.length; i++) {
			System.out.print(sol[i]);
		}
		System.out.println();
	}
	
	public static void print_cluster(Schema[] schema) {
		for (int i = 0; i < schema.length; i++) {
			System.out.print(schema[i].get_wildcard_num() + " " + schema[i].get_avg_fitness() + " " + schema[i].get_best_fitness() + " ");
			byte[] s = schema[i].get_schema();
			for (int j = 0; j < s.length; j++) {
				if (s[j] == 0)
					System.out.print('0');
				if (s[j] == 1)
					System.out.print('1');
				if (s[j] == 2)
					System.out.print('*');
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) throws Exception{
		Problem p = new Problem("maxcut500.txt", 1L);
		routine(p);
	}
}
