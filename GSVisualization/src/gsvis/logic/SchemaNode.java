package gsvis.logic;

import gsvis.logic.SchemaNode;
import gsvis.model.NodeModel;

import java.util.ArrayList;
import java.util.List;

public class SchemaNode {
	private Schema schema;
	int n;
	private Integer fitness;
	private NodeModel model;
	private List<SchemaNode> child;
	private SchemaNode parent;
	private SearchAlg search_instance;
	public void resetChild()
	{
		child=null;
		System.gc();
		child=new ArrayList<SchemaNode>();
	}
	public Integer dontcare()
	{
		int c=0;
		if(schema==null)return n;
		byte[] sc=schema.get_schema();
		for(int i=0;i<sc.length;i++)
		{
			if(sc[i]==2)c++;
		}
		return c;
	}
	public Double getAvg()
	{
		if(schema==null)return 0.0;
		return schema.get_avg_fitness();
	}
	public Double getBest()
	{
		if(schema==null)return 0.0;
		return schema.get_best_fitness();
	}
	public SearchAlg getSearchInstance()
	{
		return search_instance;
	}
	public void setModel(NodeModel m)
	{
		model=m;
	}
	public NodeModel getModel()
	{
		return model;
	}
	public void setParent(SchemaNode schemaNode)
	{
		parent=schemaNode;
	}
	public SchemaNode getParent()
	{
		return parent;
	}
	
	public SchemaNode(Schema name, SearchAlg instance)
	{
		schema=name;
		n=name.get_schema().length;
		child=new ArrayList<SchemaNode>();
		this.search_instance=instance;
	}
	public SchemaNode(String name, SearchAlg instance)
	{
		schema=null;
		n=name.length();
		child=new ArrayList<SchemaNode>();
		this.search_instance=instance;
	}
	
	
	public void clear()
	{
		schema=null;
		fitness=null;
		child=null;
		System.gc();
	}
	public void clearChild()
	{
		child=null;
		child=new ArrayList<SchemaNode>();
	}
	public void addChild(SchemaNode c)
	{
		child.add(c);
		c.setParent(this);
	}
	public int getChildNum()
	{
		if(child==null)
		{
			return 0;
		}
		return child.size();
	}
	public SchemaNode getChild(int index)
	{
		return child.get(index);
	}
	
	
	
	
	
	public void setFitness(Integer value)
	{
		fitness=value;
	}
	
	public Schema getSchema()
	{
		return schema;
	}
	public String getSchemaString()
	{
		String t="";
		if(schema==null)
		{
			for(int i=0;i<n;i++)
			{
				t=t+"*";
			
			
			}
			return t;
		}
		
		byte[] sc=schema.get_schema();
		for(int i=0;i<n;i++)
		{
			if(sc[i]==0)
			{
				t=t+"0";
			}
			if(sc[i]==1)
			{
				t=t+"1";
			}
			if(sc[i]==2)
			{
				t=t+"*";
			}
		}
		return t;
	}
	
	public Integer getFitness()
	{
		return fitness;
	}
}
