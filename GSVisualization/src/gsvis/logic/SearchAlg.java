package gsvis.logic;

import java.util.*;

public class SearchAlg {
	private static final int[] const_exp = {0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80, 0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000, 0x4000, 0x8000};
	private static final int MAX_L = 15;
	// private static final int L = 1; // component size (number of genes in one component)
	private final Random R;
	
	// problem info
	private final Problem p;
	private final int vnum;
	private final int[] deg;
	private final int[][] adj, weight;
	
	// buf
	private int[] fval = new int[const_exp[MAX_L]];
	
	// nonstatic
	private final byte[] cur, best; // solution
	private int cur_val, best_val; // fitness
	private int offset, cnt_all, cnt_loc; // sampler's variables
	private boolean updated; // sampler's variables
	private int m; // temperature-like variable
	private int escape, min_cut; // variable for problem-specific policy (control m)
	private int gen_step = 0;
	
	private int capacity = 200;
	private PriorityQueue<Chromosome> pq = new PriorityQueue<Chromosome>();
	private HashMap<Chromosome, Object> h = new HashMap<Chromosome, Object>();
	
	private SearchAlg parent = null;
	private byte[] range;
	private int[] wildcards;
	private LinkedList<TabuPattern> tabu = new LinkedList<TabuPattern>();
	
	public SearchAlg(Problem p) {
		// SET RANDOM SEED
		R = p.R;
		
		// LOAD INPUT
		this.p = p;
		vnum = p.vnum;
		deg = p.deg;
		adj = p.adj;
		weight = p.weight;
		
		cur = new byte[vnum]; best = new byte[vnum];
		
		range = new byte[vnum];
		wildcards = new int[vnum];
		for (int i = 0; i < vnum; i++) {
			range[i] = 2;
			wildcards[i] = i;
		}
		
		// negate for find mincut
		for (int i = 0; i < vnum; i++) {
			for (int j = 0; j < vnum; j++) {
				weight[i][j] = -weight[i][j];
			}
		}

		// SIMPLE HEURISTIC FOR FINDING DECENT MIN-CUT
		reset_sampler(0);
		while (true) {
			int res = gibbs();
			if (cur_val > best_val) {
				best_val = cur_val;
				updated = true;
			}
			cnt_all++;
			if (res != 0) cnt_loc++;
			offset++; // offset += L;
			if (offset >= vnum) {
				offset -= vnum;
				if (!updated) {
					if (cnt_all == cnt_loc) break;
					else m++;
				}
				updated = false;
				cnt_all = 0; cnt_loc = 0;
			}
			
		}
		min_cut = -best_val;
		
		// RESTORE EDGE WEIGHTS & CALCULATE
		int swsum = 0;
		for (int i = 0; i < vnum; i++) {
			for (int j = 0; j < vnum; j++) {
				weight[i][j] = -weight[i][j];
				swsum += Math.abs(weight[i][j]);
			}
		}
		escape = (int) Math.pow(swsum, 0.5);
		
		// SET VARIABLES FOR SEARCH ALGORITHM
		reset_sampler(min_cut);
	}
	
	private SearchAlg(Problem p, byte[] init, byte[] range, int escape, int min_cut) {
		// SET RANDOM SEED
		R = p.R;
		
		// LOAD INPUT
		this.p = p;
		vnum = p.vnum;
		deg = p.deg;
		adj = p.adj;
		weight = p.weight;
		
		cur = Arrays.copyOf(init, vnum); best = Arrays.copyOf(init, vnum);
		cur_val = eval(cur); best_val = cur_val;
		offset = 0; cnt_all = 0; cnt_loc = 0;
		updated = false;
		m = 0;
		
		this.escape = escape;
		this.min_cut = min_cut;
		
		this.range = range;
		int w = 0;
		for (int i = 0; i < vnum; i++) {
			if (range[i] == 2) w++;
		}
		wildcards = new int[w];
		w = 0;
		for (int i = 0; i < vnum; i++) {
			if (range[i] == 2) {
				wildcards[w] = i;
				w++;
			}
		}
	}
	
	private void reset_sampler(int m) {
		for (int i = 0; i < vnum; i++) {
			cur[i] = 0; best[i] = 0;
		}
		cur_val = 0; best_val = 0;
		offset = 0; cnt_all = 0; cnt_loc = 0;
		updated = false;
		this.m = m;
	}
	
	public void onestep() {
		int res = gibbs();
		
		if (cur_val > best_val) {
			best_val = cur_val;
			for (int i = 0; i < vnum; i++) {
				best[i] = cur[i];
			}
			updated = true;
		}
		SearchAlg n = parent;
		while (n != null) {
			if (cur_val > n.best_val){
				n.best_val = cur_val;
				for (int i = 0; i < vnum; i++) {
					n.best[i] = cur[i];
				}
			}
			n = n.parent;
		}
		
		cnt_all++;
		if (res != 0) cnt_loc++;
		offset++; // offset += L;
		if (offset >= wildcards.length) {
			offset -= wildcards.length;
			if (!updated) {
				if (cnt_all == cnt_loc) m = Math.max(m - escape, min_cut);
				else m++;
			}
			updated = false;
			cnt_all = 0; cnt_loc = 0;
		}
		
		// warning (slower)
		Chromosome c = new Chromosome(get_cur_sol(), cur_val);
		n = this;
		//while (n != null) {
			if (!n.h.containsKey(c)) {
				n.h.put(c, new Object());
				n.pq.add(c);
				if (n.pq.size() > n.capacity) {
					n.h.remove(n.pq.poll());
				}
			}
			n = n.parent;
		//}
		
		n = this;
		while (n != null) {
			n.gen_step++;
			n = n.parent;
		}
	}
	
	public byte[] get_cur_sol() {
		return Arrays.copyOf(cur, cur.length);
	}
	
	public byte[] get_best_sol() {
		return Arrays.copyOf(best, cur.length);
	}
	
	public int get_cur_fval() {
		return cur_val;
	}
	
	public int get_best_fval() {
		return best_val;
	}
	
	public void set_capacity(int c) {
		if (c < capacity) {
			for (int i = 0; i < capacity - c; i++) {
				h.remove(pq.poll());
			}
		}
		capacity = c;
	}
	
	public int get_capacity() {
		return capacity;
	}
	
	public byte[][] get_best_history() {
		PriorityQueue<Chromosome> copy = new PriorityQueue<Chromosome>(pq);
		byte[][] ret = new byte[copy.size()][];
		for (int i = 0; !copy.isEmpty(); i++) {
			ret[i] = copy.poll().genes;
		}
		return ret;
	}
	
	public Schema[] get_schema_cluster() {
		PriorityQueue<Chromosome> copy = new PriorityQueue<Chromosome>(pq);
		SchemaImp[] history = new SchemaImp[copy.size()];
		for (int i = 0; !copy.isEmpty(); i++) {
			history[i] = new SchemaImp(copy.poll());
		}
		int n = history.length;
		int[][] d = new int[n][n];
		int[] dmin = new int[n];
		int[] dmind = new int[n];
		for (int i = 0; i < n; i++) {
			d[i][i] = Integer.MAX_VALUE;
			dmin[i] = i;
			for (int j = i + 1; j < n; j++) {
				d[i][j] = history[i].getdist(history[j]);
				d[j][i] = d[i][j];
			}
			
			for (int j = 0; j < n; j++) {
				if (d[i][j] < d[i][dmin[i]]) dmin[i] = j;
			}
			dmind[i] = d[i][dmin[i]];
		}
		
		SchemaImp[] internal = new SchemaImp[n - 1];
		for (int s = 0; s < n - 1; s++) {
			int i1 = 0;
			for (int i = 0; i < n; i++) {
				if (dmind[i] < dmind[i1]) i1 = i;
			}
			int i2 = dmin[i1];
			SchemaImp schema = new SchemaImp(history[i1], history[i2]);
			internal[s] = schema;
			history[i1] = schema;
			history[i2] = null;
			dmin[i1] = i1;
			dmin[i2] = i2;
			dmind[i2] = Integer.MAX_VALUE;
			
			// update d and dmin[i1]
			for (int j = 0; j < n; j++) {
				if (j == i1 || history[j] == null) continue;
				d[i1][j] = schema.getdist(history[j]);
				d[i2][j] = Integer.MAX_VALUE;
				d[j][i1] = d[i1][j];
				d[j][i2] = d[i2][j];
				if (d[i1][j] < d[i1][dmin[i1]]) dmin[i1] = j;
			}
			dmind[i1] = d[i1][dmin[i1]];
			
			// update other dmin
			for (int i = 0; i < n; i++) {
				if (i == i1 || i == dmin[i]) continue;
				if (dmin[i] == i2) dmin[i] = i1;
				
				if (dmin[i] == i1) {
					if (dmind[i] < d[i][i1]) {
						for (int j = 0; j < n; j++) {
							if (d[i][j] < d[i][i1]) {
								dmin[i] = i1;
							}
						}
					}
				} else {
					if (d[i][i1] < d[i][dmin[i]]) dmin[i] = i1;
				}
				dmind[i] = d[i][dmin[i]];
			}
		}
		
		Arrays.sort(internal);
		double allowance = internal[0].priority / 1.8;
		int num = 0;
		for (int i = 0; i < internal.length; i++) {
			if (internal[i].priority < allowance && i >= 3) break;
			num++;
		}
		Schema[] ret = new Schema[num];
		for (int i = 0; i < num; i++) {
			ret[i] = internal[i];
		}
		return ret;
	}
	
	public SearchAlg subproblem(Schema schema) {
		/*
		byte[] init = new byte[vnum];
		for (int i = 0; i < vnum; i++) {
			if (schema[i] == 2) {
				init[i] = (byte) R.nextInt(2);
			} else init[i] = schema[i];
		}
		*/
		SearchAlg s = new SearchAlg(p, schema.get_best(), schema.get_schema(), escape, min_cut);
		s.parent = this;
		return s;
	}
	
	public void add_tabu(Schema schema) {
		byte[] sch = schema.get_schema();
		TabuPattern t = new TabuPattern(schema);
		if (t.error == 0) {
			int[] avail = new int[vnum];
			int w = 0;
			for (int i = 0; i < vnum; i++) {
				Iterator<TabuPattern> it = tabu.iterator();
				int cnt = 0;
				while (it.hasNext()) {
					TabuPattern tmp = it.next();
					if (!tmp.flip_permitted(i)) break;
					cnt++;
				}
				if (cnt != tabu.size()) break;
				if (range[i] < 2 && range[i] == cur[i]) break;
				if (sch[i] < 2 && sch[i] != cur[i]) break;
				avail[w] = i;
				w++;
			}
			if (w == 0) return; // infeasible
			tabu.add(t);
			int r = R.nextInt(w);
			cur[avail[r]] = (byte) (1 - cur[avail[r]]);
			Iterator<TabuPattern> it = tabu.iterator();
			while (it.hasNext()) {
				it.next().update_error(avail[r]);
			}
		} else {
			tabu.add(t);
		}

		LinkedList<Chromosome> tmp = new LinkedList<Chromosome>();
		while (!pq.isEmpty()) {
			Chromosome c = pq.poll();
			h.remove(c);
			if (!t.contains(c.genes)) tmp.add(c);
		}
		while (!tmp.isEmpty()) {
			Chromosome c = tmp.poll();
			h.put(c, new Object());
			pq.add(c);
		}
	}
	
	public void remove_tabu(Schema schema) {
		byte[] sch = schema.get_schema();
		Iterator<TabuPattern> it = tabu.iterator();
		while (it.hasNext()) {
			byte[] g = it.next().genes;
			int cnt = 0;
			for (int i = 0; i < vnum; i++) {
				if (sch[i] == g[i]) cnt++;
				else break;
			}
			if (cnt == vnum) {
				it.remove();
				break;
			}
		}
	}
	
	public int get_generation_num() {
		return gen_step;
	}

	public Schema[] get_tabu_list() {
		Iterator<TabuPattern> it = tabu.iterator();
		Schema[] ret = new Schema[tabu.size()];
		int i = 0;
		while (it.hasNext()) {
			ret[i] = it.next().s;
			i++;
		}
		return ret;
	}
	
	public int eval(byte[] c) {
		int sum = 0;
		for (int v = 0; v < vnum; v++) {
			for (int i = 0; i < deg[v]; i++) {
				int w = adj[v][i];
				if (w > v && c[v] != c[w]) {
					sum += weight[v][w];
				}
			}
		}
		return sum;
	}
	
	// check if x_v is a fixed var
	private boolean fixed(int v, int offset, int l) {
		// l = 1
		/*
		if (offset + l <= wildcards.length) {
			return (v < offset) || (v >= offset + l);
		} else {
			return (v >= offset + l - wildcards.length) && (v < offset);
		}
		*/
		return v != wildcards[offset];
	}
	
	private void recur(int[] var, int[] s0, int[] s1, int depth, int l, int f, int id) {
		// assert: 0 <= id < 2^depth
		if (depth == l) {
			fval[id] = f;
			return;
		}
		int v = var[depth];
		int t0 = 0, t1 = 0;
		for (int i = 0; i < depth; i++) {
			int w = var[i];
			if ((id & const_exp[i]) == 0) {
				t0 += weight[v][w];
			} else {
				t1 += weight[v][w];
			}
		}

		// case 1: x_v = 0
		recur(var, s0, s1, depth + 1, l, f + s1[depth] + t1, id);

		// case 2: x_v = 1
		recur(var, s0, s1, depth + 1, l, f + s0[depth] + t0, id | const_exp[depth]);
	}
	
	/* GIBBS SAMPLING (O(deg_1 + ... + deg_l) + 2^l)
	 return values
	 0: normally end
	 1: P(cur) = 1
	 -1: P(X) = 0 for all 2^l possibilities (no changes) */
	private int gibbs() {
		Iterator<TabuPattern> it = tabu.iterator();
		while (it.hasNext()) {
			if (!it.next().flip_permitted(wildcards[offset])) {
				return -1;
			}
		}
		
		int l = 1; // int l = L < 1 ? 1 : L;
		int[] var = new int[l];
		int[][] s = new int[2][l];
		for (int i = 0; i < l; i++) {
			var[i] = wildcards[(offset + i) % wildcards.length];
			s[0][i] = 0;
			s[1][i] = 0;
		}
		for (int i = 0; i < l; i++) {
			int v = var[i];
			for (int j = 0; j < deg[v]; j++) {
				int w = adj[v][j];
				if (fixed(w, offset, l)) {
					s[cur[w]][i] += weight[v][w];
				}
			}
		}

		int tmp_val = cur_val;
		for (int i = 0; i < l; i++) {
			int v = var[i];
			tmp_val -= s[1 ^ cur[v]][i];
		}
		for (int i = 0; i < l; i++) {
			int v = var[i];
			for (int j = i + 1; j < l; j++) {
				int w = var[j];
				if (cur[v] != cur[w]) {
					tmp_val -= weight[v][w];
				}
			}
		}

		recur(var, s[0], s[1], 0, l, tmp_val, 0);

		int exp = const_exp[l];
		int sum = 0;
		for (int i = 0; i < exp; i++) {
			int ratio = Math.max(fval[i] - m, 0);
			sum += ratio;
		}

		if (sum == 0) return -1;

		int cur_id = 0;
		for (int i = 0; i < l; i++) {
			if (cur[var[i]] != 0) {
				cur_id = cur_id | const_exp[i];
			}
		}
		if (sum == Math.max(fval[cur_id] - m, 0)) {
			return 1;
		}

		int r = R.nextInt(sum); // r < sum
		int sel = -1;
		for (int i = 0; i < exp; i++) {
			int ratio = fval[i] > m ? fval[i] - m : 0;
			if (r < ratio) {
				sel = i;
				break;
			} else {
				r -= ratio;
			}
		}
		if (sel < 0) {
			// unreachable
			sel = 0;
		}
		for (int i = 0; i < l; i++) {
			int v = var[i];
			int bef = cur[v];
			cur[v] = (sel & const_exp[i]) > 0 ? (byte) 1 : 0;
			if (cur[v] != bef) {
				it = tabu.iterator();
				while (it.hasNext()) {
					it.next().update_error(v);
				}
			}
		}
		cur_val = fval[sel];
		return 0;
	}
	
	private class Chromosome implements Comparable<Chromosome> {
		private final byte[] genes;
		private final int fitness;
		
		public Chromosome(byte[] genes, int fitness) {
			this.genes = genes;
			this.fitness = fitness;
		}
		
		public int hashCode() {
			int ret = 0;
			for (int i = 0; i < genes.length; i++) {
				ret = (ret << 1) | genes[i];
			}
			return ret;
		}
		
		public boolean equals(Object o) {
			if (!(o instanceof Chromosome)) return false;
			Chromosome c = (Chromosome) o;
			if (genes.length != c.genes.length) return false;
			for (int i = 0; i < genes.length; i++) {
				if (genes[i] != c.genes[i]) return false;
			}
			return true;
		}

		public int compareTo(Chromosome c) {
			return fitness - c.fitness;
		}
	}

	private class SchemaImp implements Comparable<SchemaImp>, Schema {
		private byte[] genes;
		private byte[] best_sol;
		private int best;
		private int sum;
		private int count;
		private int wildcard;
		private double priority;
		
		public SchemaImp(Chromosome c) {
			genes = c.genes;
			best = c.fitness;
			best_sol = c.genes;
			sum = c.fitness;
			count = 1;
			wildcard = 0;
		}
		
		public SchemaImp(SchemaImp l, SchemaImp r) {
			genes = new byte[vnum];
			wildcard = 0;
			for (int i = 0; i < vnum; i++) {
				if (l.genes[i] == r.genes[i]) genes[i] = l.genes[i];
				else {
					genes[i] = 2;
					if (l.genes[i] < 2 && r.genes[i] < 2); // TODO (generalization penalty)
				}
				if (genes[i] == 2) wildcard++;
			}
			if (l.best > r.best) {
				best = l.best;
				best_sol = l.best_sol;
			} else {
				best = r.best;
				best_sol = r.best_sol;
			}
			sum = l.sum + r.sum;
			count = l.count + r.count;
			double weight = 0;
			if (wildcard < wildcards.length / 1.5) {
				weight = 5.0 * wildcard / wildcards.length;
			}
			priority = weight * count;
		}
		
		public int getdist(SchemaImp s) {
			int dist = 0;
			for (int i = 0; i < vnum; i++) {
				if (genes[i] != s.genes[i]) {
					if (genes[i] < 2 && s.genes[i] < 2) dist += 4; // TODO (adjust weight: because more generalized pattern could absorb all things)
					else dist++;
				}
			}
			return dist;
		}
		
		public byte[] get_schema() {
			return genes;
		}
		
		public byte[] get_best() {
			return best_sol;
		}
		
		public double get_avg_fitness() {
			return (double) sum / count;
		}
		
		public double get_best_fitness() {
			return best;
		}
		
		public int get_wildcard_num() {
			return wildcard;
		}
		
		public String toString() {
			char[] s = new char[vnum];
			for (int i = 0; i < vnum; i++) {
				if (genes[i] == 0) s[i] = '0';
				else if (genes[i] == 1) s[i] = '1';
				else s[i] = '*';
			}
			return new String(s);
		}

		public int compareTo(SchemaImp s) {
			if (priority > s.priority) return -1;
			else if (priority < s.priority) return 1;
			return 0;
		}
	}
	
	private class TabuPattern {
		private byte[] genes;
		private int error;
		private Schema s;
		
		public TabuPattern(Schema s) {
			this.genes = s.get_schema();
			error = 0;
			for (int i = 0; i < vnum; i++) {
				if (genes[i] < 2 && cur[i] != genes[i]) error++;
			}
			this.s = s;
		}
		
		public boolean contains(byte[] sol) {
			for (int i = 0; i < vnum; i++) {
				if (genes[i] < 2 && sol[i] != genes[i]) return false;
			}
			return true;
		}
		
		public boolean flip_permitted(int index) {
			if (error == 1 && genes[index] < 2 && genes[index] != cur[index]) return false;
			return true;
		}
		
		public void update_error(int index) {
			// called only after flipped
			if (genes[index] == 2) return;
			if (genes[index] == cur[index]) error--;
			else error++;
		}
	}
}
