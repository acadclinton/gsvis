package gsvis.logic;
public interface Schema {
	public double get_avg_fitness();
	public double get_best_fitness();
	public byte[] get_schema();
	public byte[] get_best();
	public int get_wildcard_num();
}
